﻿Public Class classNetwork
    Inherits classPlayer
    Dim Path As String = "\\KUMMER2\Writable Folder\Game Networking"
    Public fName As String
    Dim eName As String
    Public x As Integer
    Public y As Integer
    Overrides Sub Start()
        Dim Temp As String
        Dim Temps() As String
        Dim Temps2() As String
        Dim intTemp() As String
        Dim i1, i2, i3 As Integer
        Dim sendShips As String = ""
        Board.Create()
        fName = "N.txt"
        eName = "P.txt"
        For i1 = 0 To 4
            sendShips &= Player.Board.Ship(i1).ToStr & "|"
        Next
        sendShips = Mid(sendShips, 1, Len(sendShips) - 1)
        IO.File.WriteAllText(Path & fName, sendShips)
        Do Until IO.File.Exists(Path & eName)

        Loop
        System.Threading.Thread.Sleep(100)
        FileOpen(1, Path & eName, OpenMode.Input)
        Temp = LineInput(1)
        Temps = Split(Temp, "|")
        For i1 = 0 To UBound(Temps)
            Temps2 = Split(Temps(i1), ";")
            intTemp = Split(Temps2(0), ",")
            Board.Ship(i1).x1 = Int(intTemp(0))
            Board.Ship(i1).y1 = Int(intTemp(1))
            intTemp = Split(Temps2(1), ",")
            Board.Ship(i1).x2 = Int(intTemp(0))
            Board.Ship(i1).y2 = Int(intTemp(1))

            For i2 = Board.Ship(i1).x1 To Board.Ship(i1).x2
                For i3 = Board.Ship(i1).y1 To Board.Ship(i1).y2
                    Board.Pos(i2, i3) = 2
                Next
            Next
        Next

        FileClose(1)
        IO.File.Delete(Path & eName)
        Do While IO.File.Exists(Path & fName)

        Loop
    End Sub

    Sub receiveTurn()
        Dim Temp() As String
        Do Until IO.File.Exists(Path & eName)

        Loop
        System.Threading.Thread.Sleep(100)
        FileOpen(1, Path & eName, OpenMode.Input)
        Temp = Split(LineInput(1), ",")
        Fire(Player.Board, Int(Temp(0)), Int(Temp(1)))
        FileClose(1)
        IO.File.Delete(Path & eName)
    End Sub
    Sub sendTurn()
        Dim turnString As String
        frmFire.ShowForm(Board)
        RedrawBoard(frmGame.picMyBoard, Player.Board)
        RedrawBoard(frmGame.picEnemyBoard, Network.Board, False)
        turnString = Trim(Str(x)) & ","
        turnString &= Trim(Str(y))
        IO.File.WriteAllText(Path & fName, turnString)
        Do While IO.File.Exists(Path & fName)

        Loop
    End Sub
End Class