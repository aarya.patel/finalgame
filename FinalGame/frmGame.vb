﻿Public Class frmGame
    Private Sub btnPlay_Click(sender As Object, e As EventArgs) Handles btnPlay.Click
        Dim allShipsSunk1 As Boolean = False
        Dim allShipsSunk2 As Boolean = False
        Dim Smh As Point
        Dim i As Integer
        Player.Start()
        frmPlaceShip.ShowForm("Carrier: (5 Long)", 5, 0)
        RedrawBoard(picMyBoard, Player.Board, 0)
        frmPlaceShip.ShowForm("Batleship: (4 Long)", 4, 1)
        RedrawBoard(picMyBoard, Player.Board, 1)
        frmPlaceShip.ShowForm("Cruiser: (3 Long)", 3, 2)
        RedrawBoard(picMyBoard, Player.Board, 2)
        frmPlaceShip.ShowForm("Submarine: (3 Long)", 3, 3)
        RedrawBoard(picMyBoard, Player.Board, 3)
        frmPlaceShip.ShowForm("Destroyer: (2 Long)", 2, 4)
        RedrawBoard(picMyBoard, Player.Board, 4)
        CurEnemy = cmbEnemy.Text
        btnPlay.Enabled = False
        cmbEnemy.Enabled = False
        Select Case CurEnemy
            Case "Network"
                Network.Start()
                RedrawBoard(picMyBoard, Player.Board)
                RedrawBoard(picEnemyBoard, Network.Board,, False)
                Select Case Network.fName
                    Case "N.txt"
                        Network.sendTurn()
                        RedrawBoard(picMyBoard, Player.Board)
                        RedrawBoard(picEnemyBoard, Network.Board,, False)
                    Case "P.txt"
                        Network.receiveTurn()
                        RedrawBoard(picMyBoard, Player.Board)
                        RedrawBoard(picEnemyBoard, Network.Board,, False)
                        Network.sendTurn()
                End Select
                RedrawBoard(picMyBoard, Player.Board)
                RedrawBoard(picEnemyBoard, Network.Board,, False)
                Do Until allShipsSunk1 Or allShipsSunk2
                    allShipsSunk1 = True
                    allShipsSunk2 = True
                    Network.receiveTurn()
                    RedrawBoard(picMyBoard, Player.Board)
                    RedrawBoard(picEnemyBoard, Network.Board,, False)
                    Network.sendTurn()
                    RedrawBoard(picMyBoard, Player.Board)
                    RedrawBoard(picEnemyBoard, Network.Board,, False)
                    For i = 0 To 4
                        If Not Network.Board.isSunk(Network.Board.Ship(i)) Then
                            allShipsSunk1 = False
                        End If
                        If Not Player.Board.isSunk(Player.Board.Ship(i)) Then
                            allShipsSunk2 = False
                        End If
                    Next
                Loop
            Case "AI"
                Computer.Start()
                RedrawBoard(picEnemyBoard, Computer.Board,, False)
                Do Until allShipsSunk1 Or allShipsSunk2
                    allShipsSunk1 = True
                    allShipsSunk2 = True
                    frmFire.ShowForm(Computer.Board)
                    RedrawBoard(picEnemyBoard, Computer.Board,, False)
                    Smh = Computer.NewTurn(Player.Board)
                    Computer.Fire(Player.Board, Smh.X, Smh.Y)
                    RedrawBoard(picMyBoard, Player.Board)
                    For i = 0 To 4
                        If Not Computer.Board.isSunk(Computer.Board.Ship(i)) Then
                            allShipsSunk1 = False
                        End If
                        If Not Player.Board.isSunk(Player.Board.Ship(i)) Then
                            allShipsSunk2 = False
                        End If
                    Next
                Loop
                btnPlay.Enabled = True
                cmbEnemy.Enabled = True

        End Select

        Leaderboard.AddPerson(Player.Score)
        Leaderboard.Output(lstLeaderboard)
    End Sub

    Private Sub cmbEnemy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbEnemy.SelectedIndexChanged
        btnPlay.Enabled = True
    End Sub

    Private Sub frmGame_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Leaderboard.Fill()
        Leaderboard.Output(lstLeaderboard)
    End Sub
End Class