﻿Module modMain
    Public Leaderboard As New classLeaderboard
    Public Player As New classPlayer
    Public Computer As New classComputer
    Public CurEnemy As String
    Public Network As New classNetwork
    Sub RedrawBoard(ByRef picBoard As PictureBox, Board As classPlayer.BoardData, Optional NumShips As Integer = 4, Optional ShowShips As Boolean = True)
        Dim x, y As Integer
        Dim x1, y1, x2, y2 As Single
        Dim newRender As New Bitmap(My.Resources.Grid)
        Dim Scale As Integer = newRender.Height / 10
        Dim ShipColor As Color = Color.FromArgb(25, 0, 0, 255)
        Using g = Graphics.FromImage(newRender)
            g.FillRectangle(New SolidBrush(Color.FromArgb(124, 255, 255, 255)), 0, 0, newRender.Width, newRender.Height)
            For x = 0 To NumShips
                If ShowShips Or Board.isSunk(Board.Ship(x)) Then
                    x1 = Board.Ship(x).x1 * Scale + 1
                    y1 = Board.Ship(x).y1 * Scale + 1
                    x2 = ((Board.Ship(x).x2 * Scale) - (x1 + 1)) + Scale
                    y2 = ((Board.Ship(x).y2 * Scale) - (y1 + 1)) + Scale
                    g.FillRectangle(New SolidBrush(ShipColor), x1, y1, x2, y2)
                    g.DrawRectangle(New Pen(Color.Blue, 4), x1, y1, x2, y2)
                End If
            Next
            For x = 0 To 9
                For y = 0 To 9
                    Select Case Board.Pos(x, y)
                        Case 1
                            g.FillRectangle(New SolidBrush(Color.Black), (x * Scale) + 20, (y * Scale) + 20, Scale - 40, Scale - 40)
                        Case 3
                            g.FillRectangle(New SolidBrush(Color.Red), (x * Scale) + 20, (y * Scale) + 20, Scale - 40, Scale - 40)
                    End Select
                Next y
            Next x
        End Using
        picBoard.Image?.Dispose()
        picBoard.Image = newRender
    End Sub
End Module