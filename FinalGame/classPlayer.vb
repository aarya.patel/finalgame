﻿Public Class classPlayer
    Inherits classLeaderboard
    Public Structure BoardData
        Public Structure ShipData
            Dim x1 As Integer
            Dim y1 As Integer
            Dim x2 As Integer
            Dim y2 As Integer
            Function ToStr() As String
                Return Trim(Str(x1)) & "," & Trim(Str(y1)) & ";" & Trim(Str(x2)) & "," & Trim(Str(y2))
            End Function
        End Structure
        Dim Pos(,) As Integer
        Dim Ship() As ShipData
        Sub Create()
            ReDim Pos(9, 9)
            ReDim Ship(4)
            Dim x, y As Integer
            For x = 0 To 9
                For y = 0 To 9
                    Pos(x, y) = 0
                Next
            Next
        End Sub
        Function isValidPos(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer) As Boolean
            Dim x, y As Integer
            Dim isValid As Boolean = True
            Dim Temp As Integer
            If x1 > x2 Then
                Temp = x1
                x1 = x2
                x2 = Temp
            End If
            If y1 > y2 Then
                Temp = y1
                y1 = y2
                y2 = Temp
            End If
            For x = x1 To x2
                For y = y1 To y2
                    If (x >= 0 And x <= 9) And (y >= 0 And y <= 9) Then
                        If Pos(x, y) = 2 Or Pos(x, y) = 3 Then
                            isValid = False
                        End If
                    Else
                        isValid = False
                    End If
                Next y
            Next x
            Return isValid
        End Function
        Function isSunk(testShip As ShipData)
            Dim x, y As Integer
            Dim Sunk As Boolean = True
            For x = testShip.x1 To testShip.x2
                For y = testShip.y1 To testShip.y2
                    If Pos(x, y) <> 3 Then
                        Sunk = False
                    End If
                Next
            Next
            Return Sunk
        End Function
    End Structure

    Public Board As BoardData
    Public Score As Integer

    Public Overridable Sub Start()
        Board.Create()
    End Sub

    Function Fire(Target As BoardData, x As Integer, y As Integer) As Boolean
        Dim Hit As Boolean
        If Target.Pos(x, y) = 2 Then
            Hit = True
            score += 10
            Target.Pos(x, y) = 3
            If My.Resources.explosion.CanRead Then
                Dim bStr(My.Resources.explosion.Length) As Byte
                My.Resources.explosion.Read(bStr, 0, My.Resources.explosion.Length)
                My.Computer.Audio.Play(bStr, AudioPlayMode.Background)
            End If
        Else
            Target.Pos(x, y) = 1
            Hit = False
            score += 1
        End If

        Return Hit
    End Function
End Class