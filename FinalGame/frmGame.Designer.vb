﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGame))
        Me.btnPlay = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.grpMyBoard = New System.Windows.Forms.GroupBox()
        Me.picMyBoard = New System.Windows.Forms.PictureBox()
        Me.grpEnemyBoard = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.picEnemyBoard = New System.Windows.Forms.PictureBox()
        Me.lblShipName = New System.Windows.Forms.Label()
        Me.cmbEnemy = New System.Windows.Forms.ComboBox()
        Me.lstLeaderboard = New System.Windows.Forms.ListBox()
        Me.grpMyBoard.SuspendLayout()
        CType(Me.picMyBoard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpEnemyBoard.SuspendLayout()
        CType(Me.picEnemyBoard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnPlay
        '
        Me.btnPlay.Enabled = False
        Me.btnPlay.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPlay.Location = New System.Drawing.Point(12, 86)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(190, 41)
        Me.btnPlay.TabIndex = 3
        Me.btnPlay.Text = "Play"
        Me.btnPlay.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 21)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "A"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 21)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "B"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 127)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 21)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "C"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 167)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(21, 21)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "D"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 207)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 21)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "E"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 247)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(18, 21)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "F"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 287)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(21, 21)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "G"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 327)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(21, 21)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "H"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 367)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(14, 21)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "I"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(15, 407)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(15, 21)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "J"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(42, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(16, 21)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "1"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(82, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(18, 21)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "2"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(122, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(18, 21)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "3"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(162, 20)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 21)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = "4"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(202, 20)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(18, 21)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = "5"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(242, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(18, 21)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "6"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(282, 20)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(18, 21)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "7"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(322, 20)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(18, 21)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "8"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(362, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(18, 21)
        Me.Label19.TabIndex = 22
        Me.Label19.Text = "9"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(402, 20)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(24, 21)
        Me.Label20.TabIndex = 23
        Me.Label20.Text = "10"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'grpMyBoard
        '
        Me.grpMyBoard.Controls.Add(Me.Label20)
        Me.grpMyBoard.Controls.Add(Me.Label19)
        Me.grpMyBoard.Controls.Add(Me.Label18)
        Me.grpMyBoard.Controls.Add(Me.Label17)
        Me.grpMyBoard.Controls.Add(Me.Label16)
        Me.grpMyBoard.Controls.Add(Me.Label15)
        Me.grpMyBoard.Controls.Add(Me.Label14)
        Me.grpMyBoard.Controls.Add(Me.Label13)
        Me.grpMyBoard.Controls.Add(Me.Label12)
        Me.grpMyBoard.Controls.Add(Me.Label11)
        Me.grpMyBoard.Controls.Add(Me.Label10)
        Me.grpMyBoard.Controls.Add(Me.Label9)
        Me.grpMyBoard.Controls.Add(Me.Label8)
        Me.grpMyBoard.Controls.Add(Me.Label7)
        Me.grpMyBoard.Controls.Add(Me.Label6)
        Me.grpMyBoard.Controls.Add(Me.Label5)
        Me.grpMyBoard.Controls.Add(Me.Label4)
        Me.grpMyBoard.Controls.Add(Me.Label3)
        Me.grpMyBoard.Controls.Add(Me.Label2)
        Me.grpMyBoard.Controls.Add(Me.Label1)
        Me.grpMyBoard.Controls.Add(Me.picMyBoard)
        Me.grpMyBoard.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMyBoard.Location = New System.Drawing.Point(232, 12)
        Me.grpMyBoard.Name = "grpMyBoard"
        Me.grpMyBoard.Size = New System.Drawing.Size(449, 458)
        Me.grpMyBoard.TabIndex = 24
        Me.grpMyBoard.TabStop = False
        Me.grpMyBoard.Text = "Your Board"
        '
        'picMyBoard
        '
        Me.picMyBoard.BackColor = System.Drawing.Color.White
        Me.picMyBoard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picMyBoard.Image = Global.FinalGame.My.Resources.Resources.Grid
        Me.picMyBoard.Location = New System.Drawing.Point(39, 47)
        Me.picMyBoard.Name = "picMyBoard"
        Me.picMyBoard.Size = New System.Drawing.Size(400, 400)
        Me.picMyBoard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picMyBoard.TabIndex = 1
        Me.picMyBoard.TabStop = False
        '
        'grpEnemyBoard
        '
        Me.grpEnemyBoard.Controls.Add(Me.Label21)
        Me.grpEnemyBoard.Controls.Add(Me.Label22)
        Me.grpEnemyBoard.Controls.Add(Me.Label23)
        Me.grpEnemyBoard.Controls.Add(Me.Label24)
        Me.grpEnemyBoard.Controls.Add(Me.Label25)
        Me.grpEnemyBoard.Controls.Add(Me.Label26)
        Me.grpEnemyBoard.Controls.Add(Me.Label27)
        Me.grpEnemyBoard.Controls.Add(Me.Label28)
        Me.grpEnemyBoard.Controls.Add(Me.Label29)
        Me.grpEnemyBoard.Controls.Add(Me.Label30)
        Me.grpEnemyBoard.Controls.Add(Me.Label31)
        Me.grpEnemyBoard.Controls.Add(Me.Label32)
        Me.grpEnemyBoard.Controls.Add(Me.Label33)
        Me.grpEnemyBoard.Controls.Add(Me.Label34)
        Me.grpEnemyBoard.Controls.Add(Me.Label35)
        Me.grpEnemyBoard.Controls.Add(Me.Label36)
        Me.grpEnemyBoard.Controls.Add(Me.Label37)
        Me.grpEnemyBoard.Controls.Add(Me.Label38)
        Me.grpEnemyBoard.Controls.Add(Me.Label39)
        Me.grpEnemyBoard.Controls.Add(Me.Label40)
        Me.grpEnemyBoard.Controls.Add(Me.picEnemyBoard)
        Me.grpEnemyBoard.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpEnemyBoard.Location = New System.Drawing.Point(687, 12)
        Me.grpEnemyBoard.Name = "grpEnemyBoard"
        Me.grpEnemyBoard.Size = New System.Drawing.Size(449, 458)
        Me.grpEnemyBoard.TabIndex = 25
        Me.grpEnemyBoard.TabStop = False
        Me.grpEnemyBoard.Text = "Enemy Board"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(402, 20)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(24, 21)
        Me.Label21.TabIndex = 23
        Me.Label21.Text = "10"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(362, 20)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(18, 21)
        Me.Label22.TabIndex = 22
        Me.Label22.Text = "9"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(322, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(18, 21)
        Me.Label23.TabIndex = 21
        Me.Label23.Text = "8"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(282, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(18, 21)
        Me.Label24.TabIndex = 20
        Me.Label24.Text = "7"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(242, 20)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(18, 21)
        Me.Label25.TabIndex = 19
        Me.Label25.Text = "6"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(202, 20)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(18, 21)
        Me.Label26.TabIndex = 18
        Me.Label26.Text = "5"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(162, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(19, 21)
        Me.Label27.TabIndex = 17
        Me.Label27.Text = "4"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(122, 20)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(18, 21)
        Me.Label28.TabIndex = 16
        Me.Label28.Text = "3"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(82, 20)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(18, 21)
        Me.Label29.TabIndex = 15
        Me.Label29.Text = "2"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(42, 20)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(16, 21)
        Me.Label30.TabIndex = 14
        Me.Label30.Text = "1"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(15, 407)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(15, 21)
        Me.Label31.TabIndex = 13
        Me.Label31.Text = "J"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(17, 367)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(14, 21)
        Me.Label32.TabIndex = 12
        Me.Label32.Text = "I"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(12, 327)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(21, 21)
        Me.Label33.TabIndex = 11
        Me.Label33.Text = "H"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(12, 287)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(21, 21)
        Me.Label34.TabIndex = 10
        Me.Label34.Text = "G"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(14, 247)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(18, 21)
        Me.Label35.TabIndex = 9
        Me.Label35.Text = "F"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(13, 207)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(18, 21)
        Me.Label36.TabIndex = 8
        Me.Label36.Text = "E"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(12, 167)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(21, 21)
        Me.Label37.TabIndex = 7
        Me.Label37.Text = "D"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(13, 127)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(20, 21)
        Me.Label38.TabIndex = 6
        Me.Label38.Text = "C"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(13, 87)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(19, 21)
        Me.Label39.TabIndex = 5
        Me.Label39.Text = "B"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Segoe UI Semilight", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(13, 47)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(20, 21)
        Me.Label40.TabIndex = 4
        Me.Label40.Text = "A"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picEnemyBoard
        '
        Me.picEnemyBoard.BackColor = System.Drawing.Color.White
        Me.picEnemyBoard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEnemyBoard.Image = Global.FinalGame.My.Resources.Resources.Grid
        Me.picEnemyBoard.Location = New System.Drawing.Point(39, 47)
        Me.picEnemyBoard.Name = "picEnemyBoard"
        Me.picEnemyBoard.Size = New System.Drawing.Size(400, 400)
        Me.picEnemyBoard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEnemyBoard.TabIndex = 1
        Me.picEnemyBoard.TabStop = False
        '
        'lblShipName
        '
        Me.lblShipName.AutoSize = True
        Me.lblShipName.Font = New System.Drawing.Font("Segoe UI", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShipName.Location = New System.Drawing.Point(5, 9)
        Me.lblShipName.Name = "lblShipName"
        Me.lblShipName.Size = New System.Drawing.Size(154, 40)
        Me.lblShipName.TabIndex = 26
        Me.lblShipName.Text = "Battleship"
        '
        'cmbEnemy
        '
        Me.cmbEnemy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEnemy.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEnemy.FormattingEnabled = True
        Me.cmbEnemy.Items.AddRange(New Object() {"AI", "Network"})
        Me.cmbEnemy.Location = New System.Drawing.Point(12, 52)
        Me.cmbEnemy.Name = "cmbEnemy"
        Me.cmbEnemy.Size = New System.Drawing.Size(190, 28)
        Me.cmbEnemy.TabIndex = 27
        '
        'lstLeaderboard
        '
        Me.lstLeaderboard.Font = New System.Drawing.Font("Consolas", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstLeaderboard.FormattingEnabled = True
        Me.lstLeaderboard.ItemHeight = 18
        Me.lstLeaderboard.Location = New System.Drawing.Point(12, 132)
        Me.lstLeaderboard.Margin = New System.Windows.Forms.Padding(2)
        Me.lstLeaderboard.Name = "lstLeaderboard"
        Me.lstLeaderboard.Size = New System.Drawing.Size(190, 328)
        Me.lstLeaderboard.TabIndex = 28
        '
        'frmGame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(856, 481)
        Me.Controls.Add(Me.lstLeaderboard)
        Me.Controls.Add(Me.cmbEnemy)
        Me.Controls.Add(Me.lblShipName)
        Me.Controls.Add(Me.grpEnemyBoard)
        Me.Controls.Add(Me.grpMyBoard)
        Me.Controls.Add(Me.btnPlay)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGame"
        Me.ShowIcon = False
        Me.Text = "Battleship"
        Me.grpMyBoard.ResumeLayout(False)
        Me.grpMyBoard.PerformLayout()
        CType(Me.picMyBoard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpEnemyBoard.ResumeLayout(False)
        Me.grpEnemyBoard.PerformLayout()
        CType(Me.picEnemyBoard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picMyBoard As PictureBox
    Friend WithEvents btnPlay As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents grpMyBoard As GroupBox
    Friend WithEvents grpEnemyBoard As GroupBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents picEnemyBoard As PictureBox
    Friend WithEvents lblShipName As Label
    Friend WithEvents cmbEnemy As ComboBox
    Friend WithEvents lstLeaderboard As ListBox
End Class
