﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlaceShip
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPlaceShip))
        Me.lblShipName = New System.Windows.Forms.Label()
        Me.cmbFirstCoord = New System.Windows.Forms.ComboBox()
        Me.cmbSecondCoord = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnPlace = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblShipName
        '
        Me.lblShipName.AutoSize = True
        Me.lblShipName.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShipName.Location = New System.Drawing.Point(7, 9)
        Me.lblShipName.Name = "lblShipName"
        Me.lblShipName.Size = New System.Drawing.Size(128, 30)
        Me.lblShipName.TabIndex = 0
        Me.lblShipName.Text = "Placeholder"
        '
        'cmbFirstCoord
        '
        Me.cmbFirstCoord.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFirstCoord.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFirstCoord.FormattingEnabled = True
        Me.cmbFirstCoord.Location = New System.Drawing.Point(177, 44)
        Me.cmbFirstCoord.Name = "cmbFirstCoord"
        Me.cmbFirstCoord.Size = New System.Drawing.Size(168, 28)
        Me.cmbFirstCoord.TabIndex = 1
        '
        'cmbSecondCoord
        '
        Me.cmbSecondCoord.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSecondCoord.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSecondCoord.FormattingEnabled = True
        Me.cmbSecondCoord.Location = New System.Drawing.Point(177, 78)
        Me.cmbSecondCoord.Name = "cmbSecondCoord"
        Me.cmbSecondCoord.Size = New System.Drawing.Size(168, 28)
        Me.cmbSecondCoord.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "First Coordinate:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 81)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Second Coordinate:"
        '
        'btnPlace
        '
        Me.btnPlace.Enabled = False
        Me.btnPlace.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPlace.Location = New System.Drawing.Point(177, 112)
        Me.btnPlace.Name = "btnPlace"
        Me.btnPlace.Size = New System.Drawing.Size(168, 41)
        Me.btnPlace.TabIndex = 5
        Me.btnPlace.Text = "Place"
        Me.btnPlace.UseVisualStyleBackColor = True
        '
        'frmPlaceShip
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(357, 165)
        Me.Controls.Add(Me.btnPlace)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbSecondCoord)
        Me.Controls.Add(Me.cmbFirstCoord)
        Me.Controls.Add(Me.lblShipName)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmPlaceShip"
        Me.ShowIcon = False
        Me.Text = "Place Ship"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblShipName As Label
    Friend WithEvents cmbFirstCoord As ComboBox
    Friend WithEvents cmbSecondCoord As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnPlace As Button
End Class
