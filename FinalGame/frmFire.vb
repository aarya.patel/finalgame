﻿Public Class frmFire
    Dim Board As classPlayer.BoardData
    Dim isClosing As Boolean

    Sub ShowForm(ByRef Target As classPlayer.BoardData)
        Board = Target
        Me.ShowDialog()
    End Sub

    Private Sub frmFire_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim x, y As Integer
        For y = 0 To 9
            For x = 0 To 9
                If Board.Pos(x, y) <> 1 And Board.Pos(x, y) <> 3 Then
                    cmbCoord.Items.Add(Chr(Asc("A") + y) & Trim(Str(x + 1)))
                End If
            Next
        Next
        isClosing = False
        btnFire.Enabled = False
    End Sub

    Private Sub cmbCoord_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCoord.SelectedIndexChanged
        btnFire.Enabled = True
    End Sub

    Private Sub frmFire_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not isClosing Then
            e.Cancel = True
        End If
    End Sub

    Private Sub btnFire_Click(sender As Object, e As EventArgs) Handles btnFire.Click
        Dim x, y As Integer
        Dim Selected As String = cmbCoord.Text
        btnFire.Enabled = True
        y = Asc(Mid(Selected, 1, 1)) - Asc("A")
        x = Val(Mid(Selected, 2)) - 1
        Player.Fire(Board, x, y)
        If CurEnemy = "Network" Then
            Network.x = x
            Network.y = y
        End If

        cmbCoord.Items.Clear()
        isClosing = True
        Me.Close()
    End Sub
End Class