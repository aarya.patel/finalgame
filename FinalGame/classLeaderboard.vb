﻿Public Class classLeaderboard
    Inherits frmGame
    Public Structure PersonData
        Public Name As String
        Public Score As Integer
    End Structure

    Public Person() As PersonData

    Public Sub Fill()
        If Not IO.File.Exists("Leaderboard.txt") Then IO.File.AppendAllText("Leaderboard.txt", """Placeholder|0""")
        Dim lines As Integer = 0
        Dim Temp As String
        Dim TempArr() As String
        FileOpen(1, "Leaderboard.txt", OpenMode.Input)
        Do Until EOF(1)
            ReDim Preserve Person(lines)
            Temp = LineInput(1)
            Temp = Mid(Temp, 2, Len(Temp) - 2)
            TempArr = Split(Temp, "|")
            Person(lines).Name = TempArr(0)
            Person(lines).Score = Val(TempArr(1))
            lines += 1
        Loop
        FileClose(1)
        Sort()
    End Sub

    Public Sub AddPerson(Score As Integer)
        ReDim Preserve Person(UBound(Person) + 1)
        Dim Name As String
        Dim i As Integer = 0

        Name = InputBox("What's your name?", "Scrabble", " ")
        Do While Name = " " Or Name.Contains("|") Or Len(Name) > 12
            If Name = " " Then
                MsgBox("Please input a name!", vbOKOnly Or vbExclamation, "Scrabble")
            ElseIf Name.Contains("|") Then
                MsgBox("Please input a name that doesn't contain ""|"" !", vbOKOnly Or vbExclamation, "Scrabble")
            ElseIf Len(Name) > 12 Then
                MsgBox("Please ensure that your name is less then 12 characters long!", vbOKOnly Or vbExclamation, "Scrabble")
            End If
            Name = InputBox("What's your name?", "Scrabble", " ")
        Loop
        If Name = vbNullString Then Exit Sub
        Person(UBound(Person)).Name = Name
        Person(UBound(Person)).Score = Score

        Sort()

        FileOpen(1, "Leaderboard.txt", OpenMode.Output)
        For i = 0 To UBound(Person)
            WriteLine(1, Person(i).Name & "|" & Trim(Str(Person(i).Score)))
        Next
        FileClose(1)
    End Sub

    Public Sub Sort()
        Dim TempPerson As PersonData
        Dim Sorted As Boolean = False
        Do Until Sorted
            Sorted = True
            For i = 0 To UBound(Person) - 1
                If Person(i).Score < Person(i + 1).Score Then
                    TempPerson = Person(i)
                    Person(i) = Person(i + 1)
                    Person(i + 1) = TempPerson
                    Sorted = False
                End If
            Next
        Loop
    End Sub

    Public Sub Output(ByRef OutputList As ListBox)
        Dim i As Integer
        Dim LenArr As Integer = UBound(Person)
        OutputList.Items.Clear()
        If LenArr > 9 Then LenArr = 9
        For i = 0 To LenArr
            OutputList.Items.Add(Trim(Str(i + 1)) & Space(3 - Len(Str(i + 1))) & ") " & Person(i).Name & Space(15 - Len(Person(i).Name)) & Person(i).Score)
        Next
    End Sub
End Class
