﻿Public Class frmPlaceShip
    Dim ShipSize, ShipNum As Integer
    Dim isClosing As Boolean = False
    Private Sub frmPlaceShip_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i, c As Integer
        For c = 0 To 9
            For i = 0 To 9
                If Player.Board.isValidPos(i, c, i, c) Then
                    cmbFirstCoord.Items.Add(Chr(c + Asc("A")) & Trim(Str(i + 1)))
                End If
            Next
        Next
        isClosing = False
    End Sub

    Private Sub cmbFirstCoord_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFirstCoord.SelectedIndexChanged
        Dim Item As String = cmbFirstCoord.Text
        Dim Column As Integer
        Dim Row As Integer
        cmbSecondCoord.Items.Clear()

        Row = Asc(Mid(Item, 1, 1)) - Asc("A")
        Column = Val(Mid(Item, 2)) - 1

        If Player.Board.isValidPos(Column, Row, Column - ShipSize, Row) Then
            cmbSecondCoord.Items.Add(Chr(Row + Asc("A")) & Trim(Str(Column - ShipSize + 1)))
        End If
        If Player.Board.isValidPos(Column, Row, Column + ShipSize, Row) Then
            cmbSecondCoord.Items.Add(Chr(Row + Asc("A")) & Trim(Str(Column + ShipSize + 1)))
        End If
        If Player.Board.isValidPos(Column, Row, Column, Row - ShipSize) Then
            cmbSecondCoord.Items.Add(Chr(Row + Asc("A") - ShipSize) & Trim(Str(Column + 1)))
        End If
        If Player.Board.isValidPos(Column, Row, Column, Row + ShipSize) Then
            cmbSecondCoord.Items.Add(Chr(Row + Asc("A") + ShipSize) & Trim(Str(Column + 1)))
        End If
        btnPlace.Enabled = False
    End Sub

    Private Sub btnPlace_Click(sender As Object, e As EventArgs) Handles btnPlace.Click
        Dim intRow1, intRow2 As Integer
        Dim intColumn1, intColumn2 As Integer
        Dim i, x As Integer
        Dim Selected1 As String = cmbFirstCoord.Text, Selected2 As String = cmbSecondCoord.Text
        Dim Temp As Integer
        intRow1 = Asc(Mid(Selected1, 1, 1)) - Asc("A")
        intRow2 = Asc(Mid(Selected2, 1, 1)) - Asc("A")
        intColumn1 = Val(Mid(Selected1, 2)) - 1
        intColumn2 = Val(Mid(Selected2, 2)) - 1
        If intRow1 > intRow2 Then
            Temp = intRow1
            intRow1 = intRow2
            intRow2 = Temp
        End If
        If intColumn1 > intColumn2 Then
            Temp = intColumn1
            intColumn1 = intColumn2
            intColumn2 = Temp
        End If
        For i = intRow1 To intRow2
            For x = intColumn1 To intColumn2
                Player.Board.Pos(x, i) = 2
            Next
        Next
        With Player.Board.Ship(ShipNum)
            .x1 = intColumn1
            .y1 = intRow1
            .x2 = intColumn2
            .y2 = intRow2
        End With
        cmbFirstCoord.Items.Clear()
        cmbSecondCoord.Items.Clear()
        btnPlace.Enabled = False
        isClosing = True
        Me.Close()
    End Sub

    Sub ShowForm(Title As String, ShipLen As Integer, ShipArrPos As Integer)
        lblShipName.Text = Title
        ShipSize = ShipLen - 1
        ShipNum = ShipArrPos
        Me.ShowDialog()
    End Sub

    Private Sub cmbSecondCoord_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSecondCoord.SelectedIndexChanged
        btnPlace.Enabled = True
    End Sub

    Private Sub frmPlaceShip_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not isClosing Then
            e.Cancel = True
        End If
    End Sub
End Class