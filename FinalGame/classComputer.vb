﻿Public Class classComputer
    Inherits classPlayer

    Overrides Sub Start()
        Dim RndX, RndY, RndB As Integer
        Dim i, x, y, temp As Integer
        Dim CreatedShip As Boolean
        Dim Size As Integer
        Dim PossibleX(-1) As Integer
        Dim PossibleY(-1) As Integer
        Dim SizeArr As Integer
        Dim X1, Y1 As Integer
        Dim X2, Y2 As Integer
        Randomize()
        Board.Create()
        For i = 0 To 4
            Select Case i
                Case 0 : Size = 5
                Case 1 : Size = 4
                Case 2 : Size = 3
                Case 3 : Size = 3
                Case 4 : Size = 2
            End Select
            Size -= 1
            CreatedShip = False
            Do Until CreatedShip
                CreatedShip = True
                Do Until Board.isValidPos(RndX, RndY, RndX, RndY)
                    Randomize()
                    RndX = Int(Rnd() * 10)
                    RndY = Int(Rnd() * 10)
                Loop
                SizeArr = -1
                If Board.isValidPos(RndX, RndY, RndX - Size, RndY) Then
                    CreatedShip = True
                    SizeArr += 1
                    ReDim Preserve PossibleX(SizeArr)
                    ReDim Preserve PossibleY(SizeArr)
                    PossibleX(SizeArr) = RndX - Size
                    PossibleY(SizeArr) = RndY
                End If
                If Board.isValidPos(RndX, RndY, RndX + Size, RndY) Then
                    CreatedShip = True
                    SizeArr += 1
                    ReDim Preserve PossibleX(SizeArr)
                    ReDim Preserve PossibleY(SizeArr)
                    PossibleX(SizeArr) = RndX + Size
                    PossibleY(SizeArr) = RndY
                End If
                If Board.isValidPos(RndX, RndY, RndX, RndY - Size) Then
                    CreatedShip = True
                    SizeArr += 1
                    ReDim Preserve PossibleX(SizeArr)
                    ReDim Preserve PossibleY(SizeArr)
                    PossibleX(SizeArr) = RndX
                    PossibleY(SizeArr) = RndY - Size
                End If
                If Board.isValidPos(RndX, RndY, RndX, RndY + Size) Then
                    CreatedShip = True
                    SizeArr += 1
                    ReDim Preserve PossibleX(SizeArr)
                    ReDim Preserve PossibleY(SizeArr)
                    PossibleX(SizeArr) = RndX
                    PossibleY(SizeArr) = RndY + Size
                End If
            Loop
            X1 = RndX
            Y1 = RndY
            RndB = Int((SizeArr + 1) * Rnd())
            X2 = PossibleX(RndB)
            Y2 = PossibleY(RndB)
            If X1 > X2 Then
                temp = X1
                X1 = X2
                X2 = temp
            End If
            If Y1 > Y2 Then
                temp = Y1
                Y1 = Y2
                Y2 = temp
            End If
            For x = X1 To X2
                For y = Y1 To Y2
                    Board.Pos(x, y) = 2
                Next
            Next
            With Board.Ship(i)
                .x1 = X1
                .y1 = Y1
                .x2 = X2
                .y2 = Y2
            End With
        Next
    End Sub

    Function NewTurn(Board As BoardData) As Point
        Randomize()
        Dim XY As Point
        Dim x, y As Integer
        Dim isValid As Boolean = False
        If Int(Rnd() * 60) < 7 Then
            Do Until isValid
                x = Int(Rnd() * 10)
                y = Int(Rnd() * 10)
                If Board.Pos(x, y) = 2 Then
                    isValid = True
                    XY.X = x
                    XY.Y = y
                End If
            Loop
        Else
            Do Until isValid
                x = Int(Rnd() * 10)
                y = Int(Rnd() * 10)
                If Board.Pos(x, y) <> 1 And Board.Pos(x, y) <> 3 Then
                    isValid = True
                    XY.X = x
                    XY.Y = y
                End If
            Loop
        End If
        Return XY
    End Function
End Class